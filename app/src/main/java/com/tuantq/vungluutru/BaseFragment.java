package com.tuantq.vungluutru;

import androidx.fragment.app.Fragment;

public class BaseFragment extends Fragment {
    protected OnActionCallBack mCallBack;

    public void setmCallBack(OnActionCallBack event) {
        this.mCallBack = event;
    }

    protected StorageCommon getStorage() {
        return App.getInstance().getStorage();
    }

}
