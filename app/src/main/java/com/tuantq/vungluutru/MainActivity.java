package com.tuantq.vungluutru;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.lang.reflect.Constructor;

public class MainActivity extends AppCompatActivity implements OnActionCallBack {

    public static final String SHARE_FILE = "saveFile.pref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
    }

    private void initView() {

        showFrg(SaveFrg.TAG);
    }

/*    @Override
    public void callBack(String key, Object... data) {

    }*/

    public void showFrg(String tag) {
        try {
            Class<?> clazz = Class.forName(tag);
            Constructor<?> constructor = clazz.getConstructor();
            BaseFragment frg = (BaseFragment) constructor.newInstance();
            frg.setmCallBack(this);

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.lnl_main, frg, tag)
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void callBack(Object object, String key) {
        String num[] = (String[]) object;
        int a = Integer.parseInt(num[0]);
        int b = Integer.parseInt(num[1]);
        switch (key) {
            case Frg1.KEY_SUM:
                Toast.makeText(this, a + b + "", Toast.LENGTH_SHORT).show();
                break;
            case Frg1.KEY_SUB:
                Toast.makeText(this, a - b + "", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        saveData();
        super.onDestroy();
    }

    private void saveData() {
        SharedPreferences pref = getSharedPreferences(SHARE_FILE, MODE_PRIVATE);
        // MODE_PRIVATE: CHỈ CHO PHÉP CHÍNH ỨNG DỤNG NÀY TRUY CẬP VÀO FILE NÀY THÔI
        // file lưu trữ theo kiểu xml <userName>tuantq</userName>
        // kiều MODE_PRIVATE sẽ ghi đè dữ liệu
        //Kiểu MODE_APPEND thì như PRIVATE nhưng cho phép lưu trữ nhiều giá trị với cùng 1 KEY

        //put data vao file
        pref.edit().putString("KEY_A", getStorage().getF01Data()[0])
                .putString("KEY_B", getStorage().getF01Data()[1])
                .apply();
    }

    private StorageCommon getStorage() {
        return App.getInstance().getStorage();
    }
}
