package com.tuantq.vungluutru;

//Là kho chứa, không đưa 1 thuộc tính nào vào cả
public final class StorageCommon {
    private String[] mF01Data;

    public StorageCommon() {
        clearData();
    }

    public void clearData() {
        mF01Data = null;
    }

    public String[] getF01Data() {
        return mF01Data;
    }

    public void setF01Data(String[] data) {
        mF01Data = data;
    }


}
