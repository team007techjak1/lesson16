package com.tuantq.vungluutru;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class SaveFrg extends BaseFragment implements View.OnClickListener {
    public static final String TAG = SaveFrg.class.getName();
    private ImageView imvAnh;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_save, container, false);

        initView(view);
        return view;

    }

    private void initView(View view) {
        view.findViewById(R.id.tv_save).setOnClickListener(this);
        view.findViewById(R.id.tv_show).setOnClickListener(this);
        imvAnh = view.findViewById(R.id.imv_anh);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_save:
                saveToSystemStorage();
                break;
            case R.id.tv_show:
                showImageFromSystemStorage();
                break;
        }
    }

    private void showImageFromSystemStorage() {
        String savePath = Environment.getDataDirectory() + "/data/" + App.getInstance().getPackageName();
        String fileName = "anh.png";

        Bitmap bitmap = BitmapFactory.decodeFile(savePath + "/" + fileName);
        imvAnh.setImageBitmap(bitmap);
    }

    private void saveToSystemStorage() {
        try {
            InputStream in = App.getInstance().getAssets().open("anh.png");
            //DataDirectory trỏ đến system storage rồi trỏ đến package
            String savePath = Environment.getDataDirectory() + "/data/" + App.getInstance().getPackageName();

            String fileName = "anh.png";
            //tạo file trỏ đến file đó
            File file = new File(savePath + "/" + fileName);

            FileOutputStream out = new FileOutputStream(file);
            byte[] buffer = new byte[1024];
            int length = in.read(buffer);

            while (length > 0) {
                out.write(buffer, 0, length);
                length = in.read(buffer);
            }

            out.close();
            in.close();
            Log.i(TAG, "Save file success!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
