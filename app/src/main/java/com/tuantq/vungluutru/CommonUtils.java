package com.tuantq.vungluutru;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class CommonUtils {
    private static final String SHARE_FILE = "shareFile.pref";

    public CommonUtils() {
//        pref = App.getInstance().getSharedPreferences(SHARE_FILE, mode);
    }

    @SuppressLint("WrongConstant")
    public void savePref(String key, String data, boolean isOverride) {
        SharedPreferences pref;
        if (!isOverride) {
            pref = App.getInstance().getSharedPreferences(SHARE_FILE, Context.MODE_PRIVATE);
        } else {
            pref = App.getInstance().getSharedPreferences(SHARE_FILE, Context.MODE_APPEND);
        }
        pref.edit().putString(key, data)
                .apply();
    }

    public void savePref(String KEY, String data) {
        savePref(KEY, data, true);
    }

    public <T> T getPref() {
        SharedPreferences pref = App.getInstance().getSharedPreferences(SHARE_FILE, Context.MODE_PRIVATE);
        T data = null;
        return data;
    }

    public void clearPref() {

    }
}
