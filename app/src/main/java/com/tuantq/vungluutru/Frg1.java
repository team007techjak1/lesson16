package com.tuantq.vungluutru;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


public class Frg1 extends BaseFragment implements View.OnClickListener {
    public static final String TAG = Frg1.class.getName();

    public static final String KEY_SUM = "KEY_SUM";
    public static final String KEY_SUB = "KEY_SUB";
    private EditText edtA;
    private EditText edtB;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_1, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        edtA = view.findViewById(R.id.edt_num_A);
        edtB = view.findViewById(R.id.edt_num_B);
        view.findViewById(R.id.tv_tinh_tong).setOnClickListener(this);
        view.findViewById(R.id.tv_tinh_hieu).setOnClickListener(this);

        initData();
    }

    private void initData() {
        //Lay Data trong SharePreference
        SharedPreferences pref = App.getInstance().getSharedPreferences(MainActivity.SHARE_FILE, Context.MODE_PRIVATE);
        String dataA = pref.getString("KEY_A", null); //Giá trị thứ 2 là default value khi không có key nào tồn tại
        String dataB = pref.getString("KEY_B", null); //Giá trị thứ 2 là default value khi không có key nào tồn tại

        getStorage().setF01Data(new String[]{dataA, dataB});
        if (getStorage().getF01Data() == null) {
            return;
        }
        edtA.setText(getStorage().getF01Data()[0]);
        edtB.setText(getStorage().getF01Data()[1]);
    }

    @Override
    public void onClick(View v) {
        String numA = edtA.getText().toString();
        String numB = edtB.getText().toString();

        String[] data = new String[]{numA, numB};
        getStorage().setF01Data(data);

        switch (v.getId()) {
            case R.id.tv_tinh_hieu:
                mCallBack.callBack(data, KEY_SUB);
                break;
            case R.id.tv_tinh_tong:
                mCallBack.callBack(data, KEY_SUM);
                break;
        }
    }
}
