package com.tuantq.vungluutru;

import android.app.Application;

public class App extends Application {
    private static App instance;
    private StorageCommon storage;
    private CommonUtils commonUtils;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        //Tạo luôn StorageCommon trong môi trường App, sống cùng App
        storage = new StorageCommon();
        commonUtils = new CommonUtils();
    }

    public StorageCommon getStorage() {
        return storage;
    }

    public static App getInstance() {
        return instance;
    }
}
